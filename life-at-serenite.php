<?php

/*

	Template Name: Life at Sérénité

*/

get_header(); ?>

	<?php get_template_part('partials/hero-css'); ?>

	<section class="hero cover parallax-background">
		<div class="content">
			<div class="wrapper">

				
			</div>
		</div>
	</section>

	<section class="hero-content" id="life">
		<div class="wrapper">
			

			<div class="bg-texture">
				<div class="pattern">
					<div class="border">

						<?php get_template_part('partials/gold-scallop'); ?>

						<?php get_template_part('partials/sub-nav'); ?>

						<div class="info">

							<h4 class="top-sub"><?php the_field('hero_sub_headline'); ?></h4>
							<h2 class="main-hed"><?php the_field('hero_headline'); ?></h2>

							<div class="metal-copy">
								<?php the_field('hero_copy'); ?>					
							</div>				
						</div>

					</div>
				</div>
			</div>	



		</div>
	</section>


	<section class="photo cover" style="background-image: url(<?php $image = get_field('photo'); echo $image['url']; ?>)">
		<div class="content">

		</div>
	</section>

	<section id="shared" class="shared-ownership">
		<div class="wrapper">
			
			<div class="info">
				
				<div class="main-content">

					<h4>Shared Ownership</h4>
					<h3><?php the_field('shared_ownership_headline'); ?></h3>
					<?php the_field('shared_ownership_copy'); ?>

				</div>	

				<div class="sidebar">
					<?php the_field('shared_ownership_bullets'); ?>
				</div>

			</div>

		</div>
	</section>

	<section id="interval" class="shared-ownership-legacy">
		<div class="wrapper">
			
			<div class="info">
				
				<div class="main-content">

					<h3><?php the_field('shared_ownership_legacy_headline'); ?></h3>
					<?php the_field('shared_ownership_legacy_copy'); ?>

					<div class="bullets">
						<?php the_field('shared_ownership_legacy_bullets'); ?>						
					</div>
				</div>	

				<div class="meta">
					<img src="<?php $image = get_field('shared_ownership_legacy_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

					<div class="cta">
						<a href="<?php the_field('shared_ownership_link'); ?>" class="btn">
							<span><?php the_field('shared_ownership_link_label'); ?></span>
						</a>
					</div>
				</div>

			</div>

		</div>
	</section>


	<section id="whole" class="whole-ownership">
		<div class="wrapper">
			
			<div class="info">
				
				<div class="main-content">

					<h4>Whole Ownership</h4>
					<h3><?php the_field('whole_ownership_headline'); ?></h3>
					<?php the_field('whole_ownership_copy'); ?>

				</div>	

				<div class="sidebar">
					<?php the_field('whole_ownership_bullets'); ?>
				</div>

			</div>

		</div>
	</section>



	<section class="map">
		<div class="content">
			<?php the_field('map'); ?>			
		</div>
	</section>

<?php get_footer(); ?>