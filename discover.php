<?php

/*

	Template Name: Discover

*/

get_header(); ?>

	<?php get_template_part('partials/hero-css'); ?>
	
	<section class="hero cover parallax-background">
		<div class="content">
			<div class="wrapper">


				
			</div>
		</div>
	</section>

	<section class="form">
		<div class="wrapper">

			<div class="form-header">
				<h2><?php the_field('form_headline'); ?></h2>

				<div class="copy">
					<?php the_field('form_copy'); ?>							
				</div>
			</div>
			
			<?php $form = get_field('form_shortcode'); echo do_shortcode($form); ?>

		</div>
	</section>

	<section class="discovery-center">
		<div class="wrapper">

			<div class="header">
				<h3><?php the_field('discovery_center_headline'); ?></h3>
				<h4><?php the_field('discovery_center_sub_headline'); ?></h4>
			</div>

			<div class="photo">
				<img src="<?php $image = get_field('discovery_center_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<?php get_template_part('partials/contact-info'); ?>

		</div>
	</section>

<?php get_footer(); ?>