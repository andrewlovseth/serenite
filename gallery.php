<?php

/*

	Template Name: Gallery

*/

get_header(); ?>

	<?php get_template_part('partials/hero-css'); ?>
	
	<section class="hero cover parallax-background">
		<div class="content">
			<div class="wrapper">


				
			</div>
		</div>
	</section>

	<section class="photo-gallery">
		<div class="wrapper">

			<div class="gallery">
								
				<?php $shortcode = get_field('gallery_shortcode'); echo do_shortcode($shortcode); ?>

			</div>

		</div>
	</section>




<?php get_footer(); ?>