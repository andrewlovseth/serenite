<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7954436/6610192/css/fonts.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/lightgallery.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">

	<header>
		<div class="wrapper">

			<section class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</section>

			<div class="menu-toggle">
				<a href="#" id="toggle">
					<div class="patty"></div>
				</a>			
			</div>

			<nav>
				<div class="nav-wrapper">
					<div class="nav-content">

						<a href="<?php echo site_url('/'); ?>" class="logo-icon">
							<img class="mobile" src="<?php bloginfo('template_directory') ?>/images/logo-icon-white.png" alt="">
							<img class="desktop" src="<?php $image = get_field('header_logo_icon', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
						
						<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
						 
						    <a href="<?php the_sub_field('link'); ?>" class="link-<?php echo sanitize_title_with_dashes(get_sub_field('link')); ?>">
						        <?php the_sub_field('label'); ?>
						    </a>

						<?php endwhile; endif; ?>

					    <a href="tel:<?php the_field('phone', 'options'); ?>" class="phone">
					        <?php the_field('phone', 'options'); ?>
					    </a>
						
					</div>
							
				</div>
			</nav>

		</div>
	</header>

	<section id="page">