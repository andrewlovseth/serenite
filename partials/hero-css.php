<style>
	section.hero {
		background-image: url(<?php $image = get_field('hero_image_mobile'); echo $image['url']; ?>);
	}

	@media screen and (min-width: 768px) {
		section.hero {
			background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);
		}
	}
</style>