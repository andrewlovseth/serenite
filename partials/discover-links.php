<div class="discover-links">
	<div class="header">
		<h4><span>Discover</span></h4>		
	</div>	

	<div class="shared">
		<a href="<?php the_field('shared_ownership_link', 'options'); ?>" class="btn">
			<span><?php the_field('shared_ownership_link_label', 'options'); ?></span>
		</a>		
	</div>

	<div class="or">
		<h4>Or</h4>		
	</div>	

	<div class="whole">
		<div class="underline">
			<a href="<?php the_field('whole_ownership_link', 'options'); ?>">
				<?php the_field('whole_ownership_link_label', 'options'); ?>
			</a>
		</div>		

	</div>
</div>