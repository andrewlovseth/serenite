<?php if(have_rows('sub_nav')): ?>

	<div class="sub-nav">

		<?php while(have_rows('sub_nav')): the_row(); ?>
	 
		    <a href="<?php the_sub_field('hash'); ?>" class="smooth">
		        <?php the_sub_field('label'); ?>
		    </a>

		<?php endwhile; ?>
		
	</div>

<?php endif; ?>