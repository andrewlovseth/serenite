<div class="contact-info">
	<div class="col address">
		<p class="icon"><?php the_field('address', 'options'); ?></p>
		<a class="directions" href="<?php the_field('directions_link', 'options'); ?>">Directions</a>
	</div>

	<div class="col phone">
		<p class="icon"><?php the_field('phone', 'options'); ?></p>
	</div>

	<div class="col email">
		<p class="icon"><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></p>
	</div>
</div>