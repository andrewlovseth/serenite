<header>
	<div class="wrapper">

		<section class="logo">
			<a href="<?php echo site_url('/'); ?>">
				<img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</section>

		<div class="menu-toggle">
			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>			
		</div>

		<nav>
			<div class="nav-wrapper">
				<a href="<?php echo site_url('/'); ?>" class="logo-icon">
					<img src="<?php $image = get_field('header_logo_icon', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
				<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
				 
				    <a href="<?php the_sub_field('link'); ?>" class="link-<?php echo sanitize_title_with_dashes(get_sub_field('link')); ?>">
				        <?php the_sub_field('label'); ?>
				    </a>

				<?php endwhile; endif; ?>

			    <a href="tel:<?php the_field('phone', 'options'); ?>" class="phone">
			        <?php the_field('phone', 'options'); ?>
			    </a>
						
			</div>
		</nav>

	</div>
</header>