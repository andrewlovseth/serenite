		<section id="join-cta">
			<div class="wrapper">
				<div class="headline">
					<h3><?php the_field('join_headline', 'options'); ?></h3>
				</div>

				<div class="cta">
					<a href="<?php echo site_url('/join/'); ?>">Join Our Team</a>
				</div>

			</div>
		</section>

		<footer>
			<div class="wrapper">

				<div class="col logo">
					<a href="<?php echo site_url('/'); ?>">
						<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>

				<div class="col contact">

					<div class="address">
						<p class="icon"><?php the_field('address', 'options'); ?></p>
						<a class="directions" href="<?php the_field('directions_link', 'options'); ?>">Directions</a>
					</div>

					<div class="phone">
						<p class="icon"><?php the_field('phone', 'options'); ?></p>
					</div>
		
					<div class="email">
						<p class="icon"><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></p>
					</div>

				</div>

				<div class="col social">
					<div class="social-wrapper">
						
						<?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>
	 
						    <a href="<?php the_sub_field('link'); ?>" rel="external">
						        <img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						    </a>

						<?php endwhile; endif; ?>

					</div>
				</div>

				<div class="col copyright">
					<?php the_field('copyright', 'options'); ?>
					
				</div>

			</div>
		</footer>

	</section><!--#page-->

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>

	<?php wp_footer(); ?>

</body>
</html>