<?php

/*

	Template Name: Our Story

*/

get_header(); ?>

	<?php get_template_part('partials/hero-css'); ?>

	<section class="hero cover parallax-background">
		<div class="content">
			<div class="wrapper">

				
			</div>
		</div>
	</section>

	<section id="story" class="hero-content">
		<div class="wrapper">
			
			<div class="bg-texture">
				<div class="pattern">
					<div class="border">

						<?php get_template_part('partials/gold-scallop'); ?>

						<?php get_template_part('partials/sub-nav'); ?>

						<div class="info">
							<h4 class="top-sub"><?php the_field('hero_sub_headline'); ?></h4>
							<h2 class="main-hed"><?php the_field('hero_headline'); ?></h2>

							<div class="metal-copy">
								<?php the_field('hero_copy'); ?>					
							</div>				
						</div>

					</div>
				</div>
			</div>	


		</div>
	</section>

	<section class="history main-content">
		<div class="wrapper">

			<h3><?php the_field('history_headline'); ?></h3>

			<div class="col col-1">
				<?php the_field('history_copy_col_1'); ?>
			</div>

			<div class="col col-2">
				<?php the_field('history_copy_col_2'); ?>
			</div>
			
		</div>
	</section>

	<section id="history" class="history-slideshow cover">
		<div class="wrapper">

			<h3><?php the_field('history_slideshow_headline'); ?></h3>

			<div class="slideshow">
				<?php if(have_rows('history_slideshow')): while(have_rows('history_slideshow')): the_row(); ?>
				 
				    <div class="slide">
				    	<div class="photo">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

				    	<div class="caption">
				    		<?php the_sub_field('caption'); ?>    		
				    	</div>        
				    </div>

				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>