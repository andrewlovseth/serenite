<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section class="hero">

		<?php if(have_rows('hero_images')): $index = 1; while(have_rows('hero_images')): the_row(); ?>

			<style>
				.slide.slide-<?php echo $index; ?> .slide-image {
					background-image: url(<?php $image = get_sub_field('image_mobile'); echo $image['url']; ?>);
				}

				@media screen and (min-width: 768px) {
					.slide.slide-<?php echo $index; ?> .slide-image {
						background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);
					}
				}
			</style>
		<?php $index++; endwhile; endif; ?>


		<div class="slides">

			<?php $videoID = get_field('hero_video_id'); ?>

			<?php if(have_rows('hero_images')): $index = 1; while(have_rows('hero_images')): the_row(); ?>
			 
			 	<div class="slide slide-<?php echo $index; ?>">
					<div class="slide-image parallax-background">

						<div class="info">
							<div class="info-wrapper">
								<h1><?php the_sub_field('headline'); ?></h1>
								<h2><?php the_sub_field('subheadline'); ?></h2>

								<div class="video-thumbnail">
									<a href="#" class="video" data-src="//player.vimeo.com/video/<?php echo $videoID; ?>?title=0&byline=0&portrait=0&autoplay=true">
										<img src="<?php bloginfo('template_directory') ?>/images/play-btn.png" alt="Play" class="play normal" />
										<img src="<?php bloginfo('template_directory') ?>/images/play-btn-hover.png" alt="Play" class="play hover" />
										<span>Explore</span>
									</a>
								</div>						
							</div>
						</div>

					</div>		 		
			 	</div>

			<?php $index++; endwhile; endif; ?>
		</div>


	</section>


	<section class="about-statement">
		<div class="wrapper">

			<div class="bg-texture">
				<div class="pattern">
					<div class="border">
						
						<div class="info">
							<?php the_field('about_statement'); ?>	
						</div>

					</div>
				</div>
			</div>	

		</div>
	</section>


	<section class="discover cover" style="background-image: url(<?php $image = get_field('discover_background_image'); echo $image['url']; ?>)">
		<div class="content">
			<div class="wrapper">

				<?php get_template_part('partials/discover-links'); ?>

			</div>
		</div>
	</section>


	<section class="location">
		<div class="wrapper">

			<div class="bg-texture">
				<div class="pattern">
					<div class="border">
						
						<div class="location-header">
							<div class="info">
								<h5 class="pre-hed">Located</h5>
								<h2 class="home"><?php the_field('location_headline'); ?></h2>							
							</div>	
						</div>

					</div>
				</div>
			</div>	

		</div>
		
		<div class="gallery">
			<?php if(have_rows('location_slideshow')): while(have_rows('location_slideshow')): the_row(); ?>
			    <div class="slide">
			    	<div class="caption">
			    		<p><?php the_sub_field('caption'); ?></p>
			    	</div>
			    	<div class="photo">
			    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>
			    </div>
			<?php endwhile; endif; ?>
		</div>

		<div class="cta">			
			<a href="<?php echo site_url('/the-poconos/'); ?>" class="btn">The Poconos</a>
		</div>

	</section>


	<section class="story">
		<div class="story-wrapper cover" style="background-image: url(<?php $image = get_field('our_story_background_image'); echo $image['url']; ?>)">
			<div class="wrapper">

				<div class="bg-texture">
					<div class="pattern">
						<div class="border">

							<div class="info">
								<div class="story-header">
									<h5 class="pre-hed">From</h5>
									<h2 class="home"><?php the_field('our_story_headline'); ?></h2>
								</div>

								<div class="metal-copy">
									<?php the_field('our_story_copy'); ?>
								</div>	

								<div class="cta">			
									<a href="<?php echo site_url('/our-story/'); ?>" class="btn">Our Story</a>
								</div>
							</div>							


						</div>
					</div>
				</div>	

			</div>
		</div>
	</section>


	<section class="discovery-center">
		<div class="wrapper">

			<div class="header">
				<h3><?php the_field('visit_headline'); ?></h3>
				<h4><?php the_field('visit_sub_headline'); ?></h4>
			</div>

			<div class="photo">
				<img src="<?php $image = get_field('visit_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<?php get_template_part('partials/contact-info'); ?>

			<div class="cta">			
				<a href="<?php echo site_url('/discover/'); ?>" class="btn">Contact Us</a>
			</div>

		</div>
	</section>


	<div id="overlay">
		<div>
			<div class="overlay-x"><span class="video-x-circle">✕</span></div>
			<div class="videoWrapper videoWrapperHide">
				<iframe width="1000" height="562" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
			</div>
		</div>
	</div>


<?php get_footer(); ?>