<?php get_header(); ?>

	<section class="hero cover" style="background-image: url(<?php $image = get_field('404_hero_image', 'options'); echo $image['url']; ?>)">
		<div class="content">
			<div class="wrapper">

				
			</div>
		</div>
	</section>

	<section class="hero-content">
		<div class="wrapper">
			

			<div class="bg-texture">
				<div class="pattern">
					<div class="border">
						
						<div class="info">
							<h4 class="top-sub">404</h4>
							<h2 class="main-hed"><?php the_field('404_headline', 'options'); ?></h2>

							<div class="metal-copy">
								<?php the_field('404_copy', 'options'); ?>
							</div>				
						</div>

					</div>
				</div>
			</div>	

		</div>
	</section>
	   	
<?php get_footer(); ?>