<?php

/*

	Template Name: Join

*/

get_header(); ?>

	<?php get_template_part('partials/hero-css'); ?>
	
	<section class="hero cover parallax-background">
		<div class="content">
			<div class="wrapper">

				
			</div>
		</div>
	</section>

	<section class="hero-content">
		<div class="wrapper">
			

			<div class="bg-texture">
				<div class="pattern">
					<div class="border">
						
						<div class="info">
							<h4 class="top-sub"><?php the_field('hero_sub_headline'); ?></h4>
							<h2 class="main-hed"><?php the_field('hero_headline'); ?></h2>

							<div class="metal-copy">
								<?php the_field('hero_copy'); ?>					
							</div>				
						</div>

					</div>
				</div>
			</div>	

		</div>
	</section>

	<section class="main-content">
		<div class="wrapper">
			
			<?php the_field('main_copy'); ?>

		</div>
	</section>

<?php get_footer(); ?>