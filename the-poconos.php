<?php

/*

	Template Name: The Poconos

*/

get_header(); ?>

	<?php get_template_part('partials/hero-css'); ?>
	
	<section class="hero cover parallax-background">
		<div class="content">
			<div class="wrapper">

				
			</div>
		</div>
	</section>

	<section class="hero-content" id="poconos">
		<div class="wrapper">

			<div class="bg-texture">
				<div class="pattern">
					<div class="border">
						
						<?php get_template_part('partials/gold-scallop'); ?>

						<?php get_template_part('partials/sub-nav'); ?>

						<div class="info">
							<h4 class="top-sub"><?php the_field('hero_sub_headline'); ?></h4>
							<h2 class="main-hed"><?php the_field('hero_headline'); ?></h2>

							<div class="metal-copy">
								<?php the_field('hero_copy'); ?>					
							</div>				
						</div>

					</div>
				</div>
			</div>	

		</div>
	</section>

	<section class="slideshow" id="numbers">

		<?php if(have_rows('numbers_slideshow')): while(have_rows('numbers_slideshow')): the_row(); ?>
		 
		    <div class="slide">
		    	<div class="info">
		    		<div class="info-wrapper">
						<h4>The Poconos by the numbers:</h4>
			    		<h3><?php the_sub_field('number'); ?></h3>
						<?php the_sub_field('fact'); ?>		    			
		    		</div>
		    	</div>

		    	<div class="photo">
		     		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		    	</div>
		    </div>

		<?php endwhile; endif; ?>

	</section>

	<section class="about-copy">
		<div class="wrapper">
			
			<div class="col col-1">
				<?php the_field('about_copy_col_1'); ?>
			</div>

			<div class="col col-2">
				<?php the_field('about_copy_col_2'); ?>
			</div>

		</div>
	</section>

	<section class="sub-hero" id="learn" style="background-image: url(<?php $image = get_field('sub_hero_image'); echo $image['url']; ?>)">
		<div class="content">
			<div class="wrapper">

				<section class="logo">
					<img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php get_template_part('partials/gold-scallop'); ?>

				</section>

				<div class="bg-texture">
					<div class="pattern">
						<div class="border">
							
							<div class="info">
								<div class="metal-copy">
									<?php the_field('sub_hero_copy'); ?>
								</div>
								

								<?php get_template_part('partials/discover-links'); ?>			
							</div>

						</div>
					</div>
				</div>	

			</div>
		</div>
	</section>

<?php get_footer(); ?>