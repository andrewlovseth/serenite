$(function() {
    var $el = $('.parallax-background');
    $(window).on('scroll', function () {
        var scroll = $(document).scrollTop();
        $el.css({
            'background-position':'50% '+(-.5*scroll)+'px'
        });
    });
});


$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){
		$('nav, header, section#page').toggleClass('open');
		return false;
	});

	// Smooth Scroll
	$('.smooth:first-of-type').smoothScroll({offset: -200});

	$('.smooth:not(:first-of-type)').smoothScroll({offset: -73});




	/* ========================================== 
	scrollTop() >= 300
	Should be equal the the height of the header
	========================================== */

	$(window).scroll(function(){
	    if ($(window).scrollTop() >= 120) {
	        $('header, nav').addClass('fixed-nav');
	    }
	    else {
	        $('header, nav').removeClass('fixed-nav');
	    }
	});

	// Homepage Carousel
	$('body.home .hero .slides').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed:  500,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
	});		


	// Homepage Carousel
	$('body.home .gallery').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed:  500,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
	});		


	// The Poconos
	$('body.page-template-the-poconos .slideshow').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed:  500,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
	});


	// Our Story
	$('body.page-template-our-story .slideshow').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed:  500,
		slidesToShow: 1,
		slidesToScroll: 1,
	});


	$("section.map").fitVids();

	
	// Close Overlay, Remove iframe src
	$('.video').on('click', function(){

		$('#overlay').css('display', 'table');

		var videoSrc = $(this).data('src');

		var iframe = $('#overlay').find('iframe');
		iframe.attr('src', videoSrc);

		return false;
	});

	$('.videoWrapper').fitVids();


	// Close Overlay, Remove iframe src
	$('#overlay .overlay-x').on('click', function(){

		$('#overlay').hide();

		var iframe = $('#overlay').find('iframe');
		iframe.attr('src','');

		return false;
	});


});